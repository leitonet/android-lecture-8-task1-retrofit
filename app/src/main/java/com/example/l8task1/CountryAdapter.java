package com.example.l8task1;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    interface CountryClickListener {
        void onClick(Country country);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<Country> list;
    private CountryClickListener countryClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            countryClickListener.onClick(list.get(position));
        }
    };

    public CountryAdapter(Context context, List<Country> list, CountryClickListener countryClickListener) {
        this.context = context;
        this.list = list;
        this.countryClickListener = countryClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Country country = list.get(position);
        holder.alpha2Code.setText(country.getAlpha2Code());
        holder.name.setText(country.getName());
        holder.region.setText(country.getRegion());
        holder.subRegion.setText(country.getSubRegion());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView alpha2Code;
        TextView name;
        TextView region;
        TextView subRegion;

        ItemClickListener itemClickListener;

        public ViewHolder(View item, final ItemClickListener itemClickListener) {
            super(item);

            this.itemClickListener = itemClickListener;

            alpha2Code = (TextView) item.findViewById(R.id.alpha2_code);
            name = (TextView) item.findViewById(R.id.name);
            region = (TextView) item.findViewById(R.id.capital);
            subRegion = (TextView) item.findViewById(R.id.population);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
