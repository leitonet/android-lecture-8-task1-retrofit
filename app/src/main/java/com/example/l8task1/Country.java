package com.example.l8task1;


import java.io.Serializable;

public class Country implements Serializable {
    private String name;
    private String capital;
    private int population;
    private String borders;
    private String timezone;
    private String region;
    private String subRegion;
    private String alpha2Code;

    public Country(String name, String capital, int population, String[] borders, String timezone, String region, String subRegion, String alpha2Code) {

        this.name = name;
        this.capital = capital;
        this.population = population;
        this.timezone = timezone;
        for(int i = 0; i < borders.length; i++) {
            this.borders = this.borders + borders[i] + ", ";
        }
        this.region = region;
        this.subRegion = subRegion;
        this.alpha2Code = alpha2Code;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public int getPopulation() {
        return population;
    }

    public String getBorders() {
        return borders;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getRegion() {
        return region;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }


}