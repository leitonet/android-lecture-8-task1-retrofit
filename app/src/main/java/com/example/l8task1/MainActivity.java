package com.example.l8task1;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements CountryAdapter.CountryClickListener {

    public static final String KEY = "person";

    RecyclerView recyclerView;
    List<Country> listOfCountries;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);

        Retrofit.getCountries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {
                listOfCountries = new ArrayList<>();
                listOfCountries = countries;
                Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getKind() == RetrofitError.Kind.HTTP) {
                    listOfCountries = new ArrayList<>();
                    Toast.makeText(getApplicationContext(), "HTTP error", Toast.LENGTH_SHORT).show();
                }
            }
        });


        CountryAdapter adapter = new CountryAdapter(this, generate(), this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onClick(Country country) {
        Intent intent = new Intent(this, CountryActivity.class);
        intent.putExtra(KEY, country);
        startActivity(intent);
    }

    private List<Country> generate() {
        List<Country> list = new ArrayList<>();
        String[] str = {"55", "55", "64564"};
        for (int i = 0; i < 15; i++) {
            list.add(new Country("USA" + i, "W" + i, 35 + i, str, "dd" + i, "dd" + i, "ddd" + i, "aaa" + i));
        }
        return list;
    }

}
